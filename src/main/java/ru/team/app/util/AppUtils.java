package ru.team.app.util;

import org.apache.logging.log4j.*;

public class AppUtils {
    public static final Logger LOGGER = LogManager.getLogger("Application");
    public static final Marker DEV_MARKER = MarkerManager.getMarker("Dev");

    public static void devLog(String message) {
        LOGGER.log(Level.INFO, DEV_MARKER, "Development mode: " + message);
    }
}
