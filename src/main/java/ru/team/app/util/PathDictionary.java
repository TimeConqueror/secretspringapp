package ru.team.app.util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathDictionary {
    public static final Path STORAGE = Paths.get("storage");
    public static final Path IMAGE_STORAGE = STORAGE.resolve(Paths.get("images"));
}
