package ru.team.app.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

public class ImageStorage {
    public static final ImageStorage EMPLOYEES = new ImageStorage("employees");

    private final File saveDir;

    public ImageStorage(String relativeSaveDir) {
        this.saveDir = PathDictionary.IMAGE_STORAGE.resolve(relativeSaveDir).toFile();

        try {
            Files.createDirectories(saveDir.toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (!saveDir.isDirectory()) {
            throw new IllegalArgumentException("Provided directory to save " + saveDir + " is not a directory!");
        }
    }

    public void saveImage(UUID id, MultipartFile image, boolean validateImage) throws IOException {
        InputStream in = image.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        org.apache.commons.io.IOUtils.copy(in, baos);
        byte[] bytes = baos.toByteArray();

        if(validateImage && isImage(in)) {
            throw new IllegalArgumentException("Provided stream is not an image!");
        }

        File fileToSave = prepareFileForWrite(Paths.get(id.toString()));

        try (OutputStream out = new FileOutputStream(fileToSave)) {
            IOUtils.copy(new ByteArrayInputStream(bytes), out);
        }
    }

    public boolean isImage(InputStream imageIn) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        org.apache.commons.io.IOUtils.copy(imageIn, baos);
        byte[] bytes = baos.toByteArray();
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));

        return image != null;
    }

    public Optional<InputStream> readImage(UUID id) throws IOException {
        File file;

        try {
            file = prepareFileForRead(Paths.get(id.toString()));
        } catch (FileNotFoundException e) {
            return Optional.empty();
        }

        return Optional.of(Files.newInputStream(file.toPath()));
    }

    private File prepareFileForWrite(Path relativePath) throws IOException {
        Path savePath = saveDir.toPath().resolve(relativePath);
        File saveFile = savePath.toFile();
        FileUtils.touch(saveFile);

        if (!Files.isWritable(savePath)) {
            throw new IOException("File " + savePath + " is not writable!");
        }

        return saveFile;
    }

    private File prepareFileForRead(Path relativePath) throws IOException {
        Path savePath = saveDir.toPath().resolve(relativePath);

        File saveFile = savePath.toFile();
        if (!saveFile.exists()) {
            throw new FileNotFoundException(savePath.toString());
        } else if (!Files.isReadable(savePath)) {
            throw new IOException("File " + savePath + " is not writable!");
        }

        return saveFile;
    }
}
