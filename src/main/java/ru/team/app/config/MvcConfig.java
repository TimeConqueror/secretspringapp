package ru.team.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.team.app.components.AppProperties;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    private final AppProperties appProperties;

    @Autowired
    public MvcConfig(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

}
