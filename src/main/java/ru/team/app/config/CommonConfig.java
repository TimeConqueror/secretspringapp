package ru.team.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.team.app.components.AppProperties;

@Configuration
public class CommonConfig {
    @Bean
    public AppProperties appProperties() {
        return new AppProperties();
    }
}
