package ru.team.app.config.security.exceptions;

import org.springframework.security.authentication.AccountStatusException;

import javax.security.auth.login.AccountException;

public class WorkPeriodException extends AccountStatusException {
    public WorkPeriodException(String msg) {
        super(msg);
    }

    public WorkPeriodException(String msg, Throwable t) {
        super(msg, t);
    }
}
