package ru.team.app.config.security;

import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import ru.team.app.repo.UserRepo;
import ru.team.app.service.UserDetailsServiceImpl;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ru.team.app.config.security.JwtProperties.*;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final UserRepo userRepo;
    private static UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    public JwtAuthorizationFilter(AuthenticationManager authManager, UserRepo userRepo, UserDetailsServiceImpl userDetailsServiceImpl) {
        super(authManager);
        this.userRepo = userRepo;
        JwtAuthorizationFilter.userDetailsServiceImpl = userDetailsServiceImpl;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(AUTHORIZATION_HEADER);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        Authentication authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);

    }

    public static UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION_HEADER);

        if (token != null) {
            String onlyToken = token.replace(JwtProperties.TOKEN_PREFIX, "").trim();

            String userName = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(onlyToken).getBody()
                    .getSubject();
            if (userName != null) {
                List<GrantedAuthority> grantedList = userDetailsServiceImpl.getGrantedList(userName);
                return new UsernamePasswordAuthenticationToken(userName, null, grantedList);
            }

            return null;
        }
        return null;
    }
}
