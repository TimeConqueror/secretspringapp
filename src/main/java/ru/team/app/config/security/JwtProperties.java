package ru.team.app.config.security;

public class JwtProperties {
    public static final String SECRET = "5HT3BYNdy8yr76NxmLgEfPySupSzhPW1HeS6ETenoB1C01OPJMKFfsi9hvrZr7wt"; // Просто рандомно сгенерированная строка
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String LOGIN_URL = "/api/1/login";
}
