package ru.team.app.config.security;

import com.google.gson.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class JwtAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().append(createErrorResponse(exception));
    }

    private String createErrorResponse(Exception e) {
        JsonObject json = new JsonObject();
        json.addProperty("timestamp", new Date().getTime());
        json.addProperty("status", HttpStatus.UNAUTHORIZED.value());

        String message;
        if(e instanceof BadCredentialsException){
            message = "Неверный логин или пароль";
        } else {
            message = e.getLocalizedMessage();
        }
        json.addProperty("message", message);

        return json.toString();
    }
}