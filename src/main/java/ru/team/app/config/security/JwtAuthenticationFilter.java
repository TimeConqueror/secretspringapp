package ru.team.app.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import ru.team.app.config.security.exceptions.WorkPeriodException;
import ru.team.app.model.Credentials;
import ru.team.app.model.Employee;
import ru.team.app.model.User;
import ru.team.app.util.AppUtils;
import ru.team.app.util.DateUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;

import static ru.team.app.config.security.JwtProperties.SECRET;
import static ru.team.app.config.security.JwtProperties.TOKEN_PREFIX;

public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    public JwtAuthenticationFilter(String url, AuthenticationManager authManager) {
        super(url);
        setAuthenticationManager(authManager);
        setAuthenticationFailureHandler(new JwtAuthenticationFailureHandler());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            Credentials credentials = new ObjectMapper().readValue(request.getInputStream(), Credentials.class);
            AppUtils.LOGGER.info("Attempting to log in with credentials: {}", credentials);

            Authentication authentication = getAuthenticationManager()
                    .authenticate(new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword(), new ArrayList<>()));

            User user = (User) authentication.getPrincipal();
            authUser(user);

            return authentication;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void authUser(User user) throws AuthenticationException {
        Employee employee = user.getEmployee();

        if (employee != null) {
            LocalDate now = LocalDate.now();
            LocalDate startWork = DateUtils.convertToLocalDate(employee.getDateStartWork());

            if (startWork.isAfter(now)) {
                throw new WorkPeriodException("Аккаунт будет доступен с " + DateUtils.toString(startWork));
            } else if (employee.getDateEndWork() != null) {
                LocalDate endWork = DateUtils.convertToLocalDate(employee.getDateEndWork());
                if (endWork.isBefore(now)) {
                    throw new WorkPeriodException("Аккаунт заблокирован c " + endWork);
                }
            }
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        AppUtils.LOGGER.info("Authentication finished successfully.");

        //truncate to like ChronoUnit.DAYS
        // means unit smaller than DAY
        // will be Zero

        //Hour Example:
        //Instant before truncate: 2018-12-30T09:24:54.630Z
        //Instant after  truncate: 2018-12-30T00:00:00Z
        Instant issuedAt = Instant.now().truncatedTo(ChronoUnit.SECONDS);
        Instant expiration = issuedAt.plus(10, ChronoUnit.DAYS);

        String token = Jwts.builder()
                .setSubject(auth.getName())
                .setIssuedAt(Date.from(issuedAt))
                .setExpiration(Date.from(expiration))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        res.getWriter().print(TOKEN_PREFIX + " " + token);

        AppUtils.LOGGER.info("Sent authentication token: " + token);
    }

}