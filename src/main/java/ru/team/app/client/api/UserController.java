package ru.team.app.client.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.team.app.client.util.RestBoundValidator;
import ru.team.app.config.security.JwtAuthorizationFilter;
import ru.team.app.model.Employee;
import ru.team.app.model.User;
import ru.team.app.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/1/users")
public class UserController {
    private final UserService userService;
    private final RestBoundValidator restBoundValidator;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService, RestBoundValidator restBoundValidator, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.restBoundValidator = restBoundValidator;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/add")
    public ResponseEntity<?> addUser(@RequestBody User user) {
        return restBoundValidator.validateWithResponse(user, userService::createUser, userIn -> {
            Map<String, String> errors = new HashMap<>();

            if (userService.alreadyExists(userIn)) {
                errors.put("username", "Пользователь " + userIn.getUsername() + " уже существует");
            }

            return errors;
        });
    }

    @GetMapping("/exact")
    public ResponseEntity<User> getEmployeeById(@RequestParam("id") Long id) {
        Optional<User> optionalUser = userService.getUserById(id);
        return optionalUser.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editUser(@RequestBody User user) {
        if (!user.getPassword().isEmpty()) {// если ввели новый пароль для юзера
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            // если не вводили, то сэтим старый.
            Optional<User> userById = userService.getUserById(user.getId());
            user.setPassword(userById.get().getPassword());
        }

        return restBoundValidator.validateWithResponse(user, userService::update);
    }

    @DeleteMapping("/delete")
    public void deleteUser(@RequestParam("id") Long id) {
        userService.deleteById(id);
    }

    @GetMapping
    public List<User> getUsersWithSettings(@RequestParam(name = "option", required = false) Option option) {
        if (option == null) {
            return userService.getAllUsers();
        } else if (option == Option.WITHOUT_BOUND_EMPLOYEE) {
            return userService.findAllWithoutEmployee();
        }

        throw new IllegalArgumentException("Unknown provided option: " + option);
    }

    @GetMapping("get-user-data")
    public CurrentUserData getUserProfile(HttpServletRequest request) {
        Authentication authentication = JwtAuthorizationFilter.getAuthentication(request);
        String username = (String) authentication.getPrincipal();
        return new CurrentUserData(userService.findUserByName(username).get());
    }

    public static class CurrentUserData {
        private final String userName;
        private final String fullName;
        private final String jobPosition;
        private final String roles;
        private final String plan;

        private final boolean hasEmployee;

        public CurrentUserData(User user) {
            roles = user.getRole().toString();
            userName = user.getUsername();
            Employee employee = user.getEmployee();

            hasEmployee = employee != null;

            if (employee != null) {
                fullName = employee.getFullName();
                jobPosition = employee.getJobPosition();
                plan = employee.getPlan();
            } else {
                fullName = null;
                jobPosition = null;
                plan = null;
            }


        }

        public String getFullName() {
            return fullName;
        }

        public String getJobPosition() {
            return jobPosition;
        }

        public String getUserName() {
            return userName;
        }

        public boolean getHasEmployee() {
            return hasEmployee;
        }

        public String getPlan() {
            return plan;
        }

        public String getRoles() {
            return roles;
        }
    }

    public enum Option {
        WITHOUT_BOUND_EMPLOYEE
    }

    @PostMapping("id-to-name")
    public Map<Long, String> getNamesFromId(@RequestBody List<Long> ids) {
        return userService.findAllById(ids).stream().collect(Collectors.toMap(User::getId, User::getUsername));
    }
}
