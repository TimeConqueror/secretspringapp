package ru.team.app.client.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team.app.model.Deal;
import ru.team.app.service.ClientService;
import ru.team.app.service.DealService;
import ru.team.app.service.EmployeeService;
import ru.team.app.service.NomenService;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("api/1/reports")
public class ReportController {
    private final DealService dealService;
    private final EmployeeService employeeService;
    private final ClientService clientService;
    private final NomenService nomenService;


    public ReportController(DealService dealService, EmployeeService employeeService, ClientService clientService, NomenService nomenService) {
        this.dealService = dealService;
        this.employeeService = employeeService;
        this.clientService = clientService;
        this.nomenService = nomenService;
    }

    @GetMapping(value = "/payments-for-today", produces = MediaType.APPLICATION_JSON_VALUE)
    public String theFactOfThePaymentsForToday() {
        JsonArray todayArray = new JsonArray();
        Iterable<Deal> allForToday = dealService.findAllForToday();
        return getString(todayArray, allForToday);
    }

    private String getString(JsonArray todayArray, Iterable<Deal> allForToday) {
        for (Deal deal : allForToday) {
            JsonObject dealJson = new JsonObject();
            dealJson.addProperty("prodName", deal.getNomen().getFullName());
            dealJson.addProperty("clientName", deal.getClient().getName());
            dealJson.addProperty("dealPrice", deal.getPrice());
            dealJson.addProperty("paymentStatus", deal.getPaymentFact());
            dealJson.addProperty("clientLink", deal.getCrmLink());
            todayArray.add(dealJson);
        }
        return todayArray.toString();
    }

    @GetMapping(value = "/payments-for-tomorrow", produces = MediaType.APPLICATION_JSON_VALUE)
    public String theFactOfThePaymentsForTomorrow() {
        JsonArray tomorrowArray = new JsonArray();
        Iterable<Deal> allForTomorrow = dealService.findAllForTomorrow();
        return getString(tomorrowArray, allForTomorrow);
    }

    @GetMapping(value = "/payments-for-week", produces = MediaType.APPLICATION_JSON_VALUE)
    public String theFactOfThePaymentForWeek() {
        JsonArray weekArray = new JsonArray();
        Iterable<Deal> allForWeek = dealService.findAllForWeek();
        for (Deal deal : allForWeek) {
            JsonObject weekDealJson = new JsonObject();
            weekDealJson.addProperty("employeeName", deal.getEmployee().getShrankFullName());
            weekDealJson.addProperty("clientName", deal.getClient().getName());
            weekDealJson.addProperty("dealPrice", deal.getPrice());
            weekDealJson.addProperty("date_start_deals", deal.getDateStartDeals().toString());
            weekDealJson.addProperty("clientLink", deal.getCrmLink());
            weekArray.add(weekDealJson);
        }
        return weekArray.toString();
    }

    @GetMapping(value = "summary-report", produces = MediaType.APPLICATION_JSON_VALUE)
    public String theSummaryReport() {
        JsonArray sumArray = new JsonArray();
        Iterable<Deal> sumReport = dealService.findSummaryReport();
        for (Deal deal : sumReport) {
            JsonObject sumJson = new JsonObject();
            sumJson.addProperty("employeeName", deal.getEmployee().getShrankFullName());
            sumJson.addProperty("paymentFact", deal.getPaymentFact());
            sumJson.addProperty("paymentStatus", deal.getPaymentStatus());
            int fact;
            if (deal.getPaymentFact() == null) {
                fact = 0;
            } else {
                fact = Integer.parseInt(deal.getPaymentFact());
            }

            int plan = Integer.parseInt(deal.getEmployee().getPlan());
            float result = fact / (float) plan * 100;
            if (result > 100) {
                result = 100;
            }
            sumJson.addProperty("plan_percent", result);//процент выполнения плана


            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd");

            int nowWeek = 6 - (int) Math.ceil((float) Integer.parseInt(formatForDateNow.format(dateNow)) / 7);

            result = (float) plan / nowWeek;
            if (result < 0) {
                result = 0;
            }

            sumJson.addProperty("plan_to_week", result);//план по неделям

            result = ((float) plan - fact) / nowWeek;
            if (result < 0) {
                result = 0;
            }
            sumJson.addProperty("plan_lost_to_week", result);//осталось до конца недели

            result = plan - fact;
            if (result < 0) {
                result = 0;
            }
            sumJson.addProperty("plan_lost_to_month", result);//месяца
            sumJson.addProperty("plan", deal.getEmployee().getPlan());
            sumArray.add(sumJson);
        }
        return sumArray.toString();
    }

//    @GetMapping(value="find-board",produces= MediaType.APPLICATION_JSON_VALUE)
//    public String theFindBoard(){
//        JsonArray boardArray=new JsonArray();
//        Iterable<Deal> boardReport=dealService.findSummaryReport();
//        for (Deal deal: boardReport){
//            JsonObject boardJson = new JsonObject();
//            boardJson.addProperty("employeeName",deal.getEmployee().getFullName());//+
//
//            boardJson.addProperty("plan",deal.getEmployee().getPlan());//процент выполненного плана
//            boardJson.addProperty("paymentFact",deal.getPaymentFact());//+
//            boardJson.addProperty("plan",deal.getEmployee().getPlan());//+
//
//
//            boardJson.addProperty("plan",deal.getEmployee().getPlan());//план до конца недели
//            boardArray.add(boardJson);
//        }
//        return boardArray.toString();
//    }


}