package ru.team.app.client.api.catalogs;

import org.springframework.web.bind.annotation.*;
import ru.team.app.model.Role;
import ru.team.app.model.User;
import ru.team.app.service.RoleService;
import ru.team.app.service.UserService;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/1/roles")
public class RoleController {
    private final RoleService roleService;
    private final UserService userService;

    public RoleController(RoleService roleService, UserService userService) {
        this.roleService = roleService;
        this.userService = userService;
    }

    @GetMapping
    @ResponseBody
    public Iterable<Role> getRole() {
        return roleService.getAllRole();
    }

    @PostMapping("from-user-id")
    public Map<Long, Set<String>> getNamesFromId(@RequestBody List<Long> clientIds) {
        Map<Long, Set<String>> userRoles = new HashMap<>();
        for (Long id : clientIds) {
            Optional<User> userById = userService.getUserById(id);
            if (userById.isPresent()) {
                User user = userById.get();
                userRoles.put(user.getId(), user.getRole().stream().map(Role::getNameRole).collect(Collectors.toSet()));
            }
        }
        return userRoles;
    }
}
