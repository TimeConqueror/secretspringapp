package ru.team.app.client.api.catalogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team.app.client.util.RestBoundValidator;
import ru.team.app.model.Client;
import ru.team.app.service.ClientService;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/1/clients")
public class ClientController {

    private final RestBoundValidator restBoundValidator;
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService, RestBoundValidator restBoundValidator) {
        this.clientService = clientService;
        this.restBoundValidator = restBoundValidator;
    }

    @GetMapping
    @ResponseBody
    public Iterable<Client> getClients() {
        return clientService.findAll();
    }

    @GetMapping("/exact")
    public Client getClientById(@RequestParam("id") Long id) {
        return clientService.findById(id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addClient(@RequestBody Client client) {
        return validate(client, clientService::add);
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editClient(@RequestBody Client client) {
        return validate(client, clientService::update);
    }

    @DeleteMapping("/delete")
    public void deleteClient(@RequestParam("id") Long id) {
        clientService.deleteById(id);
    }

    @PostMapping("id-to-name")
    public Map<Long, String> getNamesFromId(@RequestBody List<Long> ids) {
        return clientService.findAllById(ids).stream().collect(Collectors.toMap(Client::getId, Client::getName));
    }

    private ResponseEntity<?> validate(Client client, Consumer<Client> onSuccess) {
        Map<String, String> errorMap = restBoundValidator.validate(client);
        if (errorMap.isEmpty()) {
            onSuccess.accept(client);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorMap);
        }
    }

}
