package ru.team.app.client.api.catalogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team.app.service.PropertyService;

import java.util.Map;

@RestController
@RequestMapping("api/1/properties")
public class PropertyController {
    private final PropertyService propertyService;

    @Autowired
    public PropertyController(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @GetMapping
    public Map<String, String> getProperties() {
        return propertyService.getAll();
    }
}
