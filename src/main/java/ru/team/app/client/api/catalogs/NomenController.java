package ru.team.app.client.api.catalogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team.app.client.util.RestBoundValidator;
import ru.team.app.model.Nomen;
import ru.team.app.service.NomenService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/1/nomen")
public class NomenController {

    private final RestBoundValidator restBoundValidator;
    private final NomenService nomenService;

    @Autowired
    public NomenController(NomenService nomenService, RestBoundValidator restBoundValidator) {
        this.nomenService = nomenService;
        this.restBoundValidator = restBoundValidator;
    }

    @GetMapping
    @ResponseBody
    public Iterable<Nomen> getNomen() {
        return nomenService.findAll();
    }

    @GetMapping("/exact")
    public Nomen getNomenById(@RequestParam("id") Long id) {
        return nomenService.findById(id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addNomen(@RequestBody Nomen nomen) {
        return restBoundValidator.validateWithResponse(nomen, nomenService::add, nomenIn -> {
            Map<String, String> errors = new HashMap<>();

            if (nomenService.findByFullName(nomenIn.getFullName()).isPresent()) {
                errors.put("fullName", "Номенклатура с названием " + nomenIn.getFullName() + " уже существует");
            }

            return errors;
        });
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editNomen(@RequestBody Nomen nomen) {
        return restBoundValidator.validateWithResponse(nomen, nomenService::update, nomenIn -> {
            Map<String, String> errors = new HashMap<>();

            Nomen dbNomen = nomenService.findById(nomen.getId());
            // если в бд уже есть запись с таким наименованием и при этом это наименование не принадлежит текущей записи
            if (nomenService.findByFullName(nomen.getFullName()).isPresent()
                    && !dbNomen.getFullName().equals(nomen.getFullName()) && dbNomen.getId().equals(nomen.getId())) {
                errors.put("fullName", "Номенклатура с названием " + nomen.getFullName() + " уже существует");
            }

            return errors;
        });
    }

    @DeleteMapping("/delete")
    public void deleteNomen(@RequestParam("id") Long id) {
        nomenService.deleteById(id);
    }

    @PostMapping("id-to-name")
    public Map<Long, String> getNamesFromId(@RequestBody List<Long> ids) {
        return nomenService.findAllById(ids).stream().collect(Collectors.toMap(Nomen::getId, nomen -> nomen.getFullName()));
    }

    private ResponseEntity<?> validate(Nomen nomen, Consumer<Nomen> onSuccess) {
        Map<String, String> errorMap = restBoundValidator.validate(nomen);
        if (errorMap.isEmpty()) {
            onSuccess.accept(nomen);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorMap);
        }
    }
}
