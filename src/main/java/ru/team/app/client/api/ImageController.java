package ru.team.app.client.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.team.app.util.ImageStorage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/1/storage/images")
public class ImageController {
    private final ImageStorage imageStorage = ImageStorage.EMPLOYEES;

    @GetMapping("/employees")
    public ResponseEntity<byte[]> getEmployeeImage(@RequestParam("id") UUID imageId) throws IOException {
        Optional<InputStream> stream = imageStorage.readImage(imageId);
        if (stream.isPresent()) {
            InputStream inputStream = stream.get();

            byte[] targetArray = new byte[inputStream.available()];
            inputStream.read(targetArray);

            byte[] encoded = Base64.getEncoder().encode(targetArray);

            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(encoded);
        }

        return ResponseEntity.notFound().build();
    }
}
