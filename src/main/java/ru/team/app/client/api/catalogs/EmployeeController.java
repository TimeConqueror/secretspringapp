package ru.team.app.client.api.catalogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.team.app.client.util.RestBoundValidator;
import ru.team.app.model.Employee;
import ru.team.app.service.EmployeeService;
import ru.team.app.util.ImageStorage;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/1/employees")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final RestBoundValidator restBoundValidator;
    private final ImageStorage imageStorage = ImageStorage.EMPLOYEES;

    @Autowired
    public EmployeeController(EmployeeService employeeService, RestBoundValidator restBoundValidator) {
        this.employeeService = employeeService;
        this.restBoundValidator = restBoundValidator;
    }

    @GetMapping
    @ResponseBody
    public Iterable<Employee> getEmployees() {
        return employeeService.findAll();
    }


    @GetMapping("/exact")
    public Employee getEmployeeById(@RequestParam("id") Long id) {
        return employeeService.findById(id);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<?> addEmployee(@RequestPart("employee") Employee employee, @RequestPart(value = "image", required = false) MultipartFile image) {
        employee.setPhotoId(null); // для предотвращения эксплойтов

        return restBoundValidator.validateWithResponse(employee, employeeIn -> {
            onValidationSuccess(employeeIn, image);

            employeeService.add(employeeIn);
        }, employeeIn -> validateImage(image));
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editEmployee(@RequestPart("employee") Employee employee, @RequestPart(value = "image", required = false) MultipartFile image) {
        Employee dbEmployee = employeeService.findById(employee.getId());
        employee.setPhotoId(dbEmployee.getPhotoId());// для предотвращения эксплойтов

        return restBoundValidator.validateWithResponse(employee, employeeIn -> {
            onValidationSuccess(employeeIn, image);

            employeeService.update(employeeIn);
        }, employeeIn -> validateImage(image));
    }

    private void onValidationSuccess(Employee employee, @Nullable MultipartFile image) {
        UUID photoId = employee.getPhotoId();

        if (image != null) {
            if(photoId == null){
                photoId = UUID.randomUUID();
            }

            try {
                imageStorage.saveImage(photoId, image, false);

                employee.setPhotoId(photoId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, String> validateImage(@Nullable MultipartFile image) {
        if (image != null) {
            String fPhotoId = "photoId";
            Map<String, String> errorMap = new HashMap<>();
            try {
                if (!imageStorage.isImage(image.getInputStream())) {
                    errorMap.put(fPhotoId, "Файл не является картинкой.");
                }
            } catch (IOException e) {
                errorMap.put(fPhotoId, "Невозможно обработать файл.");
            }

            return errorMap;
        }

        return Collections.emptyMap();
    }

    @DeleteMapping("/delete")
    public void deleteEmployee(@RequestParam("id") Long id) {
        employeeService.deleteById(id);
    }

    @PostMapping("id-to-name")
    public Map<Long, String> getNamesFromId(@RequestBody List<Long> ids) {

        return employeeService.findAllById(ids).stream().collect(Collectors.toMap(employee1 -> employee1.getId(),
                employee -> employee.getShrankFullName()));
    }
}
