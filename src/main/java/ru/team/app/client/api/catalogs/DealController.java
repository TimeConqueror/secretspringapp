package ru.team.app.client.api.catalogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team.app.client.util.RestBoundValidator;
import ru.team.app.model.Deal;
import ru.team.app.service.DealService;

import java.util.Map;
import java.util.function.Consumer;


@RestController
@RequestMapping("api/1/deals")
public class DealController {

    private final RestBoundValidator restBoundValidator;
    private final DealService dealService;

    @Autowired
    public DealController(DealService dealService, RestBoundValidator restBoundValidator) {
        this.dealService = dealService;
        this.restBoundValidator = restBoundValidator;
    }

    @GetMapping
    @ResponseBody
    public Iterable<Deal> getDeal() {
        return dealService.findAll();
    }

    @GetMapping("/exact")
    public Deal getDealById(@RequestParam("id") Long id) {
        return dealService.findById(id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addDeal(@RequestBody Deal deal) {
        return validate(deal, dealService::add);
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editDeal(@RequestBody Deal deal) {
        return validate(deal, dealService::update);
    }

    @DeleteMapping("/delete")
    public void deleteDeal(@RequestParam("id") Long id) {
        dealService.deleteById(id);
    }


    private ResponseEntity<?> validate(Deal deal, Consumer<Deal> onSuccess) {
        Map<String, String> errorMap = restBoundValidator.validate(deal);
        if (errorMap.isEmpty()) {
            onSuccess.accept(deal);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorMap);
        }
    }
}
