package ru.team.app.client.api.catalogs;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/1")
public class MainController {

    @GetMapping
    public String getMenu() {
        return "Hello!";
    }

}
