package ru.team.app.client.util;

import org.hibernate.validator.HibernateValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.team.app.util.Pair;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Валидатор, настроенный таким образом, чтобы все невалидные свойства, которые отправляются на клиент,
 * соответствовали JSON-сериализации этого объекта.
 * Например, у нас есть поле user в каком-то классе. Библиотека FasterXML с помощью json-аннотаций превращает это в userId.
 * Эот валидатор умеет распознавать подобное, поэтому при выводе всех ошибок валидации, мы увидим, что ошибка показывается в
 * свойстве userId, а не user.
 */
@Component
public class RestBoundValidator {
    private final Validator validator;

    public RestBoundValidator() {
        ValidatorFactory factory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .propertyNodeNameProvider(new JacksonPropertyNodeNameProvider())
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    public Validator getValidator() {
        return validator;
    }

    public <T> Map<String, String> validate(T obj, Function<T, Map<String, String>> additionalValidator) {
        Set<ConstraintViolation<T>> violations = getValidator().validate(obj);

        Map<String, String> errorMap = new HashMap<>(additionalValidator.apply(obj));

        boolean valid = violations.isEmpty() && errorMap.isEmpty();

        if (valid) {
            return Collections.emptyMap();
        } else {
            errorMap.putAll(violations.stream()
                    .map(violation -> Pair.of(violation.getPropertyPath().toString(), violation.getMessage()))
                    .collect(Collectors.toMap(Pair::getA, Pair::getB)));
            return errorMap;
        }
    }

    public <T> Map<String, String> validate(T obj) {
        return validate(obj, t -> Collections.emptyMap());
    }

    public <T> ResponseEntity<?> validateWithResponse(T obj, Consumer<T> onSuccess) {
        return validateWithResponse(obj, onSuccess, t -> Collections.emptyMap());
    }

    public <T> ResponseEntity<?> validateWithResponse(T obj, Consumer<T> onSuccess, Function<T, Map<String, String>> additionalValidator) {
        Map<String, String> errorMap = validate(obj, additionalValidator);
        if (errorMap.isEmpty()) {
            onSuccess.accept(obj);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorMap);
        }
    }
}
