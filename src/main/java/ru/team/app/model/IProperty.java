package ru.team.app.model;

public interface IProperty {
    String getKey();
    String getValue();
}
