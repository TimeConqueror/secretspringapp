package ru.team.app.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.format.annotation.DateTimeFormat;
import ru.team.app.components.Storage;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Embeddable
@Table(name = "deals")
public class Deal extends IdBased {

    //имя продукта сделки
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nomen_id", nullable = false, referencedColumnName = "id")
    @NotNull(message = "Введите название продукта")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("nomenId")
    private Nomen nomen;

    //колличество этого продукта
    @NotEmpty(message = "Введите количество продукта")
    @Size(min = 0)
    @Column(name = "number_of_products", nullable = false)
    private String numberOfProducts;

    //Дата сделки
    @NotNull(message = "Дата сделки должна быть выбрана")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_start_deals")
    private Date dateStartDeals;

    //Имя клиента
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = false, referencedColumnName = "id")
    @NotNull(message = "Нужно выбрать клиента из предложенного списка")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("clientId")
    private Client client;

    //имя сотрудника
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false, referencedColumnName = "id")
    @NotNull(message = "Нужно выбрать сотрудника из предложенного списка")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("employeeId")
    private Employee employee;

    //вероятности на сумму сделки
    @Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @Nullable
    @Size(min = 0)
    @Column(name = "probability10", nullable = true)
    private String probability10;

    @Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @Nullable
    @Size(min = 0)
    @Column(name = "probability50", nullable = true)
    private String probability50;

    @Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @Nullable
    @Size(min = 0)
    @Column(name = "probability100", nullable = true)
    private String probability100;

    //цена
    @Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @NotEmpty(message = "Поле не должно быть пустым, введите значение суммы")
    @Size(min = 0)
    @Column(name = "price", nullable = false)
    private String price;
    //факт оплаты
    @Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @Nullable
    @Size(min = 0)
    @Column(name = "paymentFact", nullable = true)
    private String paymentFact;

    //Статус оплаты
    @Nullable
    @Size(min = 0)
    @Column(name = "paymentStatus", nullable = true)
    private String paymentStatus;
    //  ссылка на CRM


    // @NotEmpty(message = "Поле не должно быть пустым, введите значение суммы")
    @Nullable
    @Size(min = 0)
    @Column(name = "crmLink", nullable = true)
    private String crmLink;

    @JsonProperty("nomenId")
    public Nomen getNomen() {
        return nomen;
    }

    public void setNomen(Nomen nomen) {
        this.nomen = nomen;
    }

    @JsonProperty("nomenId")
    public void setNomenById(Long nomenId) {
        nomen = Storage.getStorage().getNomenService().findById(nomenId);
    }

    public String getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(String numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public Date getDateStartDeals() {
        return dateStartDeals;
    }

    public void setDateStartDeals(@Nullable Date dateStartDeals) {
        this.dateStartDeals = dateStartDeals;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @JsonProperty("clientId")
    public void setClientById(Long clientId) {
        client = Storage.getStorage().getClientService().findById(clientId);
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @JsonProperty("employeeId")
    public void setEmployeeById(Long employeeId) {
        employee = Storage.getStorage().getEmployeeService().findById(employeeId);
    }

    @Nullable
    public String getPaymentFact() {
        return paymentFact;
    }

    public void setPaymentFact(String fact) {
        this.paymentFact = fact;
    }

    @Nullable
    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(@Nullable String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCrmLink() {
        return crmLink;
    }

    public void setCrmLink(String crmLink) {
        this.crmLink = crmLink;
    }

    @Nullable
    public String getProbability10() {
        return probability10;
    }

    public void setProbability10(@Nullable String probability10) {
        this.probability10 = probability10;
    }

    @Nullable
    public String getProbability50() {
        return probability50;
    }

    public void setProbability50(@Nullable String probability50) {
        this.probability50 = probability50;
    }

    @Nullable
    public String getProbability100() {
        return probability100;
    }

    public void setProbability100(@Nullable String probability100) {
        this.probability100 = probability100;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Deal{" +
                "nomen=" + nomen +
                ", numberOfProducts='" + numberOfProducts + '\'' +
                ", dateStartDeals=" + dateStartDeals +
                ", client=" + client +
                ", employee=" + employee +
                ", probability10='" + probability10 + '\'' +
                ", probability50='" + probability50 + '\'' +
                ", probability100='" + probability100 + '\'' +
                ", price='" + price + '\'' +
                ", paymentFact='" + paymentFact + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", crmLink='" + crmLink + '\'' +
                '}';
    }
}
