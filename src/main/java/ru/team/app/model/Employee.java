package ru.team.app.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;
import ru.team.app.components.Storage;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "employees")
public class Employee extends IdBased {
    /**
     * Имя
     */
    @NotEmpty(message = "Введите имя сотрудника")
    @Size(min = 2, max = 15)
    @Column(name = "name", nullable = false)
   // @Pattern(regexp = "^[а-яА-Я]+(?:-[а-яА-Я]+)$", message = "Поле 'Имя' содержит некорректные символы")
    private String name;
    /**
     * Фамилия
     */
    @NotEmpty(message = "Введите фамилию сотрудника")
    @Size(min = 2, max = 25)
    @Column(name = "surname", nullable = false)
   // @Pattern(regexp = "^[а-яА-Я]+(?:-[а-яА-Я]+)$", message = "Поле 'Фамилия' содержит некорректные символы")
    private String surname;
    /**
     * Отчество
     */
    @NotEmpty(message = "Введите отчество сотрудника")
    @Size(min = 2, max = 25)
    @Column(name = "patronymic", nullable = false)
   // @Pattern(regexp = "^[а-яА-Я]+(?:-[а-яА-Я]+)$", message = "Поле 'Отчество' содержит некорректные символы")
    private String patronymic;

    //дата начала работы
    @NotNull(message = "Выберите дату начала работы")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_start_work", nullable = false)
    private Date dateStartWork;

    //дата увольнения
    @Nullable
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_end_work")
    private Date dateEndWork;

    //должность
    @NotEmpty(message = "Введите должность сотрудника")
    @Size(min = 2, max = 35)
    @Column(name = "job_position", nullable = false)
    private String jobPosition;

    //фотография
    @Type(type = "pg-uuid")
    @Column(name = "photo_id", columnDefinition = "uuid")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID photoId;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = "user_id", unique = true, nullable = false, referencedColumnName = "id")
    @NotNull(message = "Выберите аккаунт для сотрудника")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("userId")
    private User user;

    //План
    @Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @NotEmpty(message = "Поле не должно быть пустым, введите значение суммы")
    @Size(min = 0)
    @Column(name = "plan", nullable = false)
    private String plan;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private final List<Deal> deals = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateStartWork() {
        return dateStartWork;
    }

    public void setDateStartWork(Date dateStartWork) {
        this.dateStartWork = dateStartWork;
    }

    @Nullable
    public Date getDateEndWork() {
        return dateEndWork;
    }

    public void setDateEndWork(@Nullable Date dateEndWork) {
        this.dateEndWork = dateEndWork;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public UUID getPhotoId() {
        return photoId;
    }

    public void setPhotoId(UUID photoId) {
        this.photoId = photoId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    @JsonProperty("userId")
    public void setUserById(Long userId) {
        user = Storage.getStorage().getUserService().getUserById(userId)
                .orElseThrow(() -> new IllegalArgumentException("No user found with id " + userId));
    }

    public String getShrankFullName() {
        return String.format("%1$s %2$s.%3$s.", getSurname(), getName().charAt(0), getPatronymic().charAt(0));
    }

    public String getFullName() {
        return String.format("%1$s %2$s %3$s", getSurname(), getName(), getPatronymic());
    }

    @Override
    public String toString() {
        return "Employee{" +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dateStartWork=" + dateStartWork +
                ", dateEndWork=" + dateEndWork +
                ", jobPosition='" + jobPosition + '\'' +
                ", plan='" + plan + '\'' +
                '}';
    }
}
