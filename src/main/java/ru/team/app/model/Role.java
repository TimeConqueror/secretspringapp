package ru.team.app.model;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "t_role")
public class Role extends IdBased {

    @NotEmpty
    @Column(name = "nameRole", nullable = false)
    private String nameRole;

    @ManyToMany(mappedBy = "role", cascade = CascadeType.REMOVE)
    private Set<User> users = new HashSet<>();

    public Role() {
        super();
    }

    public Role(String nameRole) {
        this.nameRole = nameRole;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(final String nameRole) {
        this.nameRole = nameRole;
    }

    @Override
    public String toString() {
        return nameRole;
    }
}
