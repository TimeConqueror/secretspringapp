package ru.team.app.model;

import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Embeddable
@Table(name = "nomen")
public class Nomen extends IdBased {

    //название продукт
    @Unique
    @NotEmpty(message = "Введите название продукта")
    @Column(name = "full_name", nullable = false, unique = true)
    private String fullName;

    //тип продукта
    @NotEmpty(message = "Выберите тип продукта")
    @Column(name = "prod_type", nullable = false)
    private String prodType;

    //цена продукта
    //@Pattern(regexp = "^[0-9]{0,}[,]{0,1}[0-9]{0,2}", message = "Вводить можно только числа")
    @NotNull(message = "Введите цену продукта")
    @Column(name = "prod_cost", nullable = false)
    private Integer prodCost;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public Integer getProdCost() {
        return prodCost;
    }

    public void setProdCost(Integer prodCost) {
        this.prodCost = prodCost;
    }

    @Override
    public String toString() {
        return "Nomen{" +
                "fullName='" + fullName + '\'' +
                ", prodType='" + prodType + '\'' +
                ", prodCost=" + prodCost +
                '}';
    }
}
