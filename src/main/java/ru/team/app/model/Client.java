package ru.team.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Embeddable
@Table(name = "clients")
public class Client extends IdBased {
    /**
     * true
     */
    @Column(name = "type", nullable = false)
    private Boolean rawType = false;//TODO move to its own enum with ids

    @NotEmpty(message = "Введите наименование клиента")
    @Size(max = 400)
    @Column(name = "name", nullable = false)
    private String name;

    @NotEmpty(message = "Введите физический адрес")
    @Size(max = 1000)
    @Column(name = "physical_address", nullable = false)
    private String physicalAddress;
    /**
     * Юридический адрес
     */
    @NotEmpty(message = "Введите юридический адрес")
    @Size(max = 1000)
    @Column(name = "legal_address", nullable = false)
    private String legalAddress;

    @NotEmpty(message = "Введите телефон состоящий из 11 цифр")
    @Size(min = 15, max = 18, message = "Телефон должен состоять из 11 цифр")
    @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", message = "Телефон должен состоять из 11 цифр)")
    @Column(name = "tel", nullable = false)
    private String tel;

    @NotEmpty(message = "Введите электронную почту клиента")
    @Pattern(message = "Не является почтой", regexp = "(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])")
    @Column(name = "mail_adress")
    private String mailAddress;

    /**
     * ИНН (Идентификационный Номер Налогоплательщика)
     */
    @NotEmpty(message = "Введите ИНН")
    @Size(min = 10, max = 10, message = "ИНН должен состоять из 10 цифр")
    @Pattern(regexp = "^[0-9]{10}", message = "КПП должен состоять из 10 цифр")
    @Column(name = "taxpayer_number", nullable = false)
    private String taxpayerNumber;

    /**
     * КПП (Код причины постановки на учет, code of reason for registration)
     * null у физ. лиц.
     */
    @NotEmpty(message = "Введите КПП")
    @Size(min = 9, max = 9, message = "КПП должен состоять из 9 цифр")
    @Pattern(regexp = "[0-9]{9}", message = "КПП должен состоять из 9 цифр")
    @Column(name = "crr", nullable = false)
    private String crr;


    public Boolean getRawType() {
        return rawType;
    }

    @JsonIgnore
    public String getType() {
        return (getRawType() ? "Юридическое" : "Физическое") + " лицо";
    }

    public void setRawType(Boolean type) {
        this.rawType = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(String legalAddress) {
        this.legalAddress = legalAddress;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getTaxpayerNumber() {
        return taxpayerNumber;
    }

    public void setTaxpayerNumber(String taxpayerNumber) {
        this.taxpayerNumber = taxpayerNumber;
    }

    public String getCrr() {
        return crr;
    }

    public void setCrr(String crr) {
        this.crr = crr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(rawType, client.rawType) &&
                Objects.equals(name, client.name) &&
                Objects.equals(physicalAddress, client.physicalAddress) &&
                Objects.equals(legalAddress, client.legalAddress) &&
                Objects.equals(tel, client.tel) &&
                Objects.equals(mailAddress, client.mailAddress) &&
                Objects.equals(taxpayerNumber, client.taxpayerNumber) &&
                Objects.equals(crr, client.crr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawType, name, physicalAddress, legalAddress, tel, mailAddress, taxpayerNumber, crr);
    }

    @Override
    public String toString() {
        return "Client{" +
                ", type=" + rawType +
                ", name='" + name + '\'' +
                ", physicalAddress='" + physicalAddress + '\'' +
                ", legalAddress='" + legalAddress + '\'' +
                ", tel='" + tel + '\'' +
                ", mailAddress='" + mailAddress + '\'' +
                ", taxpayerNumber='" + taxpayerNumber + '\'' +
                ", crr='" + crr + '\'' +
                '}';
    }
}
