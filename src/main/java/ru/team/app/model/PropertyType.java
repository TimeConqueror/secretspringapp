package ru.team.app.model;

import java.util.*;

public enum PropertyType {
    COMPANY_NAME(new DefaultProperty("company_name", "Название фирмы"));

    private final DefaultProperty defaultProperty;
    private static final Map<String, PropertyType> KEY_LOOKUP;

    static {
        PropertyType[] values = PropertyType.values();
        KEY_LOOKUP = new HashMap<>(values.length);
        for (PropertyType prop : values) {
            if (KEY_LOOKUP.put(prop.defaultProperty.getKey(), prop) != null) {
                throw new IllegalStateException("Key " + prop.defaultProperty.getKey() + " is already defined!");
            }
        }
    }

    PropertyType(DefaultProperty defaultProperty) {
        this.defaultProperty = defaultProperty;
    }

    public DefaultProperty getDefaultProperty() {
        return defaultProperty;
    }

    public String getKey() {
        return defaultProperty.getKey();
    }

    public static Optional<PropertyType> byKey(String key) {
        return Optional.ofNullable(KEY_LOOKUP.get(key));
    }

    public static Set<String> getKeys(){
        return KEY_LOOKUP.keySet();
    }
}
