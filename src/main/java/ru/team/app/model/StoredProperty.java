package ru.team.app.model;


import javax.persistence.*;

@Entity
@Table(name = "properties")
public class StoredProperty {
    @Id
    @Column(name = "key", unique = true, updatable = false)
    private String key;

    @Column(name = "value")
    private String value;

    public StoredProperty(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public StoredProperty(String key) {
        this.key = key;
    }

    protected StoredProperty() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
