package ru.team.app.model;

public class DefaultProperty implements IProperty {
    private final String key;
    private final String value;

    public DefaultProperty(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }
}
