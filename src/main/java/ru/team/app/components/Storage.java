package ru.team.app.components;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.team.app.service.*;

import javax.annotation.PostConstruct;

@Component
public class Storage {
    private static Storage instance;

    private final UserService userService;
    private final ClientService clientService;
    private final NomenService nomenService;
    private final EmployeeService employeeService;
    private final RoleService roleService;

    @Autowired
    private Storage(UserService userService, ClientService clientService, NomenService nomenService, EmployeeService employeeService, RoleService roleService) {
        this.userService = userService;
        this.clientService = clientService;
        this.nomenService = nomenService;
        this.employeeService = employeeService;
        this.roleService = roleService;
    }

    public UserService getUserService() {
        return userService;
    }

    public ClientService getClientService() { return clientService;}

    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    public NomenService getNomenService() {
        return nomenService;
    }

    public RoleService getRoleService(){
        return roleService;
    }

    @PostConstruct
    private void init(){
        instance = this;
    }

    public static Storage getStorage() {
        return instance;
    }
}
