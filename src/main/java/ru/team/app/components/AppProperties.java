package ru.team.app.components;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.team.app.util.AppUtils;

@ConfigurationProperties("app.environment")
public class AppProperties {
    private boolean inDev = false;

    public boolean isInDev() {
        return inDev;
    }

    public void setInDev(boolean inDev) {
        this.inDev = inDev;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onStart() {
        if (isInDev()) {
            AppUtils.LOGGER.warn("App has been started with development mode!");
        }
    }
}
