package ru.team.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.team.app.model.Client;

@Repository
public interface ClientRepo extends JpaRepository<Client, Long> {
}