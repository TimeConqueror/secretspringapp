package ru.team.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team.app.model.Role;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByNameRole(String nameRole);

}
