package ru.team.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.team.app.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    User findByUsername(String username);

    @Query(nativeQuery = true, value =
            "SELECT * FROM t_user " +
                    "WHERE NOT EXISTS ( " +
                    "SELECT * FROM employees " +
                    "WHERE employees.user_id = t_user.id " +
                    ");")
    List<User> findAllWithoutEmployee();
}