package ru.team.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.team.app.model.Employee;


@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {


}
