package ru.team.app.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.team.app.model.Deal;

import java.time.LocalDate;
import java.util.Date;


@Repository
public interface DealRepo extends CrudRepository<Deal, Long> {

//    void deleteAllByEmployeeId(Long userID);
//
//    void deleteAllByClientId(Long clientID);
//
//   void deleteAllByNomenId(Long productID);

    Iterable<Deal> findAllByNomenId(Long id);

    Iterable<Deal> findAllByClientId(Long id);

    Iterable<Deal> findAllByEmployeeId(Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM deals WHERE deals.date_start_deals = current_date")
    Iterable<Deal> findAllForToday();

    @Query(nativeQuery = true, value = "SELECT * FROM deals WHERE deals.date_start_deals =current_date + 1")
    Iterable<Deal> findAllForTomorrow();

    @Query(nativeQuery = true, value = "SELECT * FROM deals d WHERE d.date_start_deals BETWEEN :start AND :end")
    Iterable<Deal> findAllForWeek(@Param("start") LocalDate weekStart, @Param("end") LocalDate weekEnd);

    @Query(nativeQuery = true, value = "SELECT * FROM deals d WHERE d.date_start_deals  BETWEEN :start AND :end")
    Iterable<Deal> findSummaryReport(@Param("start") Date start, @Param("end") Date end);

/*	@Query(value = "SELECT * FROM Users u WHERE u.status = :status and u.name = :name",
	  nativeQuery = true)
	User findUserByStatusAndNameNamedParamsNative(
	  @Param("status") Integer status, @Param("name") String name);*/

    @Query(nativeQuery = true, value = "SELECT * FROM deals  ")
    Iterable<Deal> findBoard();

}
