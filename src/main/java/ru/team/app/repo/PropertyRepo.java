package ru.team.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team.app.model.StoredProperty;

public interface PropertyRepo extends JpaRepository<StoredProperty, String> {
}
