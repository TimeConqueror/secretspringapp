package ru.team.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.team.app.model.Nomen;

import java.util.Optional;

@Repository
public interface NomenRepo extends JpaRepository<Nomen, Long> {
    Optional<Nomen> findByFullName(String fullName);
}
