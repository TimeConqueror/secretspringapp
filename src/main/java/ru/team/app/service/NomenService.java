package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.app.model.Nomen;
import ru.team.app.repo.NomenRepo;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class NomenService implements SimpleService<Nomen, Long> {

    private final NomenRepo nomenRepo;
    private DealService dealService;

    @Autowired
    public NomenService(NomenRepo nomenRepo) {
        this.nomenRepo = nomenRepo;
    }

    @Autowired
    public void setDealService(DealService dealService) {
        this.dealService = dealService;
    }

    public Nomen findById(Long id) {
        return nomenRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Nomen with id " + id + " doesn't exist"));
    }

    public Optional<Nomen> findByFullName(String fullName) {
        return nomenRepo.findByFullName(fullName);
    }

    public Nomen add(Nomen nomen) {
        nomen.setId(null);
        return nomenRepo.save(nomen);
    }

    public Nomen update(Nomen nomen) {
        findById(nomen.getId());
        return nomenRepo.save(nomen);
    }

    public void deleteById(Long id) {
//         dealService.deleteAllByNomenId(id);
        nomenRepo.deleteById(id);
    }

    @Override
    public void delete(Nomen nomen) {
        deleteById(nomen.getId());
    }

    @Override
    public Iterable<Nomen> findAll() {
        return nomenRepo.findAll();
    }

    public List<Nomen> findAllById(Iterable<Long> ids) {
        return nomenRepo.findAllById(ids);
    }
}
