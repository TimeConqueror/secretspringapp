package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.team.app.model.Role;
import ru.team.app.model.User;
import ru.team.app.repo.RoleRepo;
import ru.team.app.repo.UserRepo;

import javax.annotation.PostConstruct;

@Service
public class PostConstBuild {

    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;

    @Autowired
    public PostConstBuild(UserRepo userRepo, PasswordEncoder passwordEncoder, RoleRepo roleRepo) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepo = roleRepo;
    }

    @PostConstruct
    private void buildAdmin() {
        try {
            String roleDirector = "director";
            Role role = this.roleRepo.findByNameRole(roleDirector);
            String roleUser = "user";
            Role role1 = this.roleRepo.findByNameRole(roleUser);
            if (role == null && role1 == null) {
                role = new Role();
                role.setNameRole(roleDirector);
                roleRepo.save(role);
                role1 = new Role();
                role1.setNameRole(roleUser);
                roleRepo.save(role1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Во время инициализации произошли ошибки.");
        }
    }

    @PostConstruct
    private void AdminCreat(){
        String usernameByAdmin = "admin";
        User user  = this.userRepo.findByUsername(usernameByAdmin);
        if (user == null) {
            user = new User();
            user.setUsername(usernameByAdmin);
            String passwordByAdmin = "123";
            user.setPassword(passwordEncoder.encode(passwordByAdmin));
            user.setEnabled(true);
            String roleAdmin = "admin";
            roleRepo.findByNameRole(roleAdmin);
            Role role2 = new Role();
            role2.setNameRole(roleAdmin);
            roleRepo.save(role2);
            user.getRole().add(role2);
            userRepo.save(user);
        }
    }
}
