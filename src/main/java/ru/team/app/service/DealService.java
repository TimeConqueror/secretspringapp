package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.app.model.Deal;
import ru.team.app.repo.DealRepo;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Service
public class DealService implements SimpleService<Deal, Long> {
    private DealRepo dealRepo;

    @Autowired
    public void setDealRepo(DealRepo dealRepo) {
        this.dealRepo = dealRepo;
    }

    public Deal findById(Long id) {
        return dealRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Deal with id " + id + " doesn't exist"));
    }

    public Deal add(Deal deal) {
        deal.setId(null);
        return dealRepo.save(deal);
    }

    public Deal update(Deal deal){
        findById(deal.getId());
        return dealRepo.save(deal);
    }

    public void deleteById(Long id){
        dealRepo.deleteById(id);
    }

    @Override
    public void delete(Deal deal) {
        deleteById(deal.getId());
    }

    @Override
    public Iterable<Deal> findAll() {
        return dealRepo.findAll();
    }

    @Transactional
    public Iterable<Deal> findAllByNomenId(Long id) {
        return dealRepo.findAllByNomenId(id);
    }

    @Transactional
    public Iterable<Deal> findAllByClientId(Long clientID) {
        return dealRepo.findAllByClientId(clientID);
    }

    @Transactional
    public Iterable<Deal> findAllByEmployeeId(Long id) {
        return dealRepo.findAllByEmployeeId(id);
    }

    public Iterable<Deal> findAllForToday() {
        return dealRepo.findAllForToday();
    }

    public Iterable<Deal> findAllForTomorrow() {
        return dealRepo.findAllForTomorrow();
    }

    public Iterable<Deal> findAllForWeek() {

        LocalDate now = LocalDate.now();

        LocalDate weekStart = now.with(DayOfWeek.MONDAY);
        LocalDate weekend = now.with(DayOfWeek.SUNDAY);
        System.out.println("первая1 дата " + weekStart);
        System.out.println("последняя1 дата " + weekend);

        return dealRepo.findAllForWeek(weekStart, weekend);
    }

    public Iterable<Deal> findSummaryReport() {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, cal.getActualMinimum(Calendar.DATE));
        Date firstDate = cal.getTime();
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        Date endDate = cal.getTime();

        return dealRepo.findSummaryReport(firstDate, endDate);
    }

    public Iterable<Deal> findBoard() {
        return dealRepo.findBoard();
    }

}
