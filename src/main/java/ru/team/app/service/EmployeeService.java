package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.app.model.Employee;
import ru.team.app.repo.EmployeeRepo;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class EmployeeService implements SimpleService<Employee, Long> {
    private EmployeeRepo repo;
    private DealService dealService;

    @Autowired
    public void setRepo(EmployeeRepo repo) {
        this.repo = repo;
    }

    @Autowired
    public void setDealService(DealService dealService) {
        this.dealService = dealService;
    }

    public Employee findById(long id) {
        return repo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Employee with id " + id + " doesn't exist"));
    }

    public Employee add(Employee employee) {
        employee.setId(null);
        return repo.save(employee);
    }

    public Employee update(Employee employee){
        findById(employee.getId());
        /*
        Когда мы подгружаем Employee, автоматически погружается User, который привязан к этому Employee.
        При этом мы меняем инстанс Employee (со старым ID), а вот User автоматически не поменяет этого,
        у него осталась ссылка на старый инстанс. Оттого возникает ошибка Multiple representations of the same entity are being merged.
         */
        employee.getUser().setEmployee(employee);
        return repo.save(employee);
    }

    public void deleteById(Long id) {
//        dealService.deleteAllByEmployeeId(id);
        repo.deleteById(id);
    }

    @Override
    public void delete(Employee employee) {
        deleteById(employee.getId());
    }

    @Override
    public Iterable<Employee> findAll() {
        return repo.findAll();
    }

    public List<Employee> findAllById(Iterable<Long> ids){
        return repo.findAllById(ids);
    }

}
