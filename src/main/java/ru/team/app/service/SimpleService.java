package ru.team.app.service;

import java.util.Optional;

public interface SimpleService<E, ID> {
//    E save(E e);

    void delete(E e);

    Iterable<E> findAll();

//    Optional<E> findById(ID id);
}