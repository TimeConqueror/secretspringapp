package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.team.app.model.User;
import ru.team.app.repo.UserRepo;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepo userRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    public Optional<User> getUserById(Long id) {
        return userRepo.findById(id);
    }

    public List<User> findAllById(Iterable<Long> ids) {
        return userRepo.findAllById(ids);
    }

    public List<User> findAllWithoutEmployee() {
        return userRepo.findAllWithoutEmployee();
    }

    public Optional<User> findUserByName(String userName) {
        return Optional.ofNullable(userRepo.findByUsername(userName));
    }

    public boolean alreadyExists(User user) {
        return userRepo.findByUsername(user.getUsername()) != null;
    }

    public boolean createUser(User user) {
        if (alreadyExists(user)) {
            return false;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepo.save(user);
        return true;
    }

    public User update(User user) {
        getUserById(user.getId());
        return userRepo.save(user);
    }

    public void deleteById(Long id) {
        userRepo.deleteById(id);
    }

    public void delete(User user) {
        deleteById(user.getId());
    }
}
