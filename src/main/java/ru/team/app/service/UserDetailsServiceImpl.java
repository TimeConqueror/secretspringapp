package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.team.app.model.Role;
import ru.team.app.model.User;
import ru.team.app.repo.UserRepo;

import javax.persistence.EntityExistsException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Autowired
    public UserDetailsServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User " + username + " not found");
        }

        return user;
    }

    public List<GrantedAuthority> getGrantedList(String username) {
        Set<Role> roleNames = userRepo.findByUsername(username).getRole();

        List<GrantedAuthority> grantedList = new ArrayList<GrantedAuthority>();
        if (roleNames != null) {
            for (Role role: roleNames) {
                String roleStr = role.getNameRole();
                GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + roleStr.toUpperCase());
                grantedList.add(authority);
            }
        }
        return grantedList;
    }
}