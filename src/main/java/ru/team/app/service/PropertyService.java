package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.app.model.IProperty;
import ru.team.app.model.PropertyType;
import ru.team.app.model.StoredProperty;
import ru.team.app.repo.PropertyRepo;

import java.util.*;

@Service
public class PropertyService {
    private final PropertyRepo propertyRepo;

    @Autowired
    public PropertyService(PropertyRepo propertyRepo) {
        this.propertyRepo = propertyRepo;
    }

    public String getValue(PropertyType type) {
        return getStoredProperty(type)
                .map(StoredProperty::getValue)
                .orElse(type.getDefaultProperty().getValue());
    }

    public Map<String, String> getAll() {
        PropertyType[] types = PropertyType.values();
        Map<String, String> values = new HashMap<>(types.length);

        for (PropertyType type : types) {
            values.put(type.getKey(), getValue(type));
        }

        return values;
    }

    private Optional<StoredProperty> getStoredProperty(PropertyType type) {
        return propertyRepo.findById(type.getKey());
    }

    public void setValueAndSave(PropertyType type, String value) {
        propertyRepo.save(new StoredProperty(type.getKey(), value));
    }
}
