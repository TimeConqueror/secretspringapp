package ru.team.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.app.model.Client;
import ru.team.app.repo.ClientRepo;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ClientService implements SimpleService<Client, Long> {
    private final ClientRepo clientRepo;
    private DealService dealService;

    @Autowired
    public ClientService(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    @Autowired
    public void setDealService(DealService dealService) {
        this.dealService = dealService;
    }

    public Client findById(long id) {
        return clientRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Client with id " + id + " doesn't exist"));
    }

    public Client add(Client client) {
        client.setId(null);
        return clientRepo.save(client);
    }

    public Client update(Client client){
        findById(client.getId());
        return clientRepo.save(client);
    }

    public void deleteById(Long id){
//        dealService.deleteAllByClientId(id);
        clientRepo.deleteById(id);
    }
    @Override
    public void delete(Client client) {
        deleteById(client.getId());
    }

    @Override
    public Iterable<Client> findAll() {
        return clientRepo.findAll();
    }

    public List<Client> findAllById(Iterable<Long> ids){
        return clientRepo.findAllById(ids);
    }
}
