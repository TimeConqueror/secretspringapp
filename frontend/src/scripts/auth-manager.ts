import axios, { AxiosInstance } from "axios";
import { Route } from "vue-router";
import { VueRouter } from "vue-router/types/router";
import { Store } from "vuex";

class AuthManager {
  public readonly onFailRedirectPath = "/login";
  private router!: VueRouter;
  private store!: Store<any>;

  /**
   * С токеном в хедере и с отлавливателем 401 и 403 ошибок.
   * К ссылкам в начале добавляется api/
   * @private
   */
  private readonly commonAxios: AxiosInstance = axios;
  /**
   * К ссылкам в начале добавляется api/
   * @private
   */
  private readonly loginBypassedAxios: AxiosInstance;

  constructor() {
    axios.defaults.baseURL = "/api/1/";
    this.loginBypassedAxios = axios.create({
      baseURL: axios.defaults.baseURL
    });
  }

  public getAxios(bypassAuthRedirect = false) {
    return bypassAuthRedirect ? this.loginBypassedAxios : this.commonAxios;
  }

  public auth(token: string) {
    localStorage.setItem("token", token);
    this.setupTokenStuff();
  }

  public logout() {
    localStorage.removeItem("token");
    axios.defaults.headers = {};

    this.store.commit("user/clearUser");
    this.router.push("/login");
  }

  /**
   * Проверяет, есть ли сохраненный токен для отправки запросов.
   * Не гарантирует то, что токен не истёк или является нерабочим.
   */
  public isAuthenticated(): boolean {
    return localStorage.getItem("token") != null;
  }

  public getCurrentRoute(): Route {
    return this.router.currentRoute;
  }

  public setup(router: VueRouter, store: Store<any>) {
    this.router = router;
    this.store = store;

    console.log("Setting up AuthManager.");

    this.setupTokenStuff();

    axios.interceptors.response.use(
      response => response,
      error => {
        console.log("AuthManager caught error: " + error.response);
        if (
          error.response.data.status === 401 ||
          error.response.data.status === 403
        ) {
          console.log("Auth error was detected, redirecting to login page...");
          this.router.push(this.onFailRedirectPath);

          return Promise.resolve();
        } else {
          console.log("Auth reject: " + error.response);
          console.log("Auth reject response data: " + error.response.data);
          return Promise.reject(error);
          // throw error;
        }
      }
    );

    this.router.beforeEach((to, from, next) => {
      if (to.path !== this.onFailRedirectPath && !this.isAuthenticated())
        next({ path: this.onFailRedirectPath });
      else next();
    });
  }

  private setupTokenStuff() {
    const token = localStorage.getItem("token");
    if (token == null) {
      console.log("Token wasn't found in local storage.");
    } else {
      console.log("Token was found in local storage. Adding to headers...");
      axios.defaults.headers = {
        Authorization: token
      };
    }

    this.store.dispatch("user/loadCurrentUser");
  }
}

const authManager: AuthManager = new AuthManager();

export default authManager;
