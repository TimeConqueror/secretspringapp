export interface Employee {
    id?: number
    surname?: string
    name?: string
    patronymic?: string
    dateStartWork?: Date
    dateEndWork?: Date
    jobPosition?: string
    photoId?: string
    userId?: number
    plan?: string
}
