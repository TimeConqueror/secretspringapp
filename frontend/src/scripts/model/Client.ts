export interface Client {
    id?: number
    rawType?: string
    name?: string
    physicalAddress?: string
    legalAddress?: string
    tel?: string
    mailAddress?: string
    taxpayerNumber?: string
    crr?: string
}
