export interface Nomen {
    id?: number
    fullName?: string
    prodType?: string
    prodCost?: string
}
