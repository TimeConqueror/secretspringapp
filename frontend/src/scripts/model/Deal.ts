export interface Deal {
    id?: number
    nomenId?: number
    numberOfProducts?: string
    dateStartDeals?: Date
    clientId?: number
    employeeId?: number
    probability10?: string
    probability50?: string
    probability100?: string
    price?: string
    paymentFact?: string
    crmLink?: string
}

