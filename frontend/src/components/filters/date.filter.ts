export interface Options {
    day?: string,
    month?: string,
    year?: string,
    hour?: string,
    minute?: string,
    second?: string
}

export default function dateFilter(value: string | number | Date, format = 'date') {
    const options: Options = {};
    if (format.includes('date')) {

        options.day = 'numeric'
        options.month = 'long'
        options.year = 'numeric'
    }
    if (format.includes('time')) {
        options.hour = 'numeric'
        options.minute = 'numeric'
        options.second = 'numeric'
    }
    return new Intl.DateTimeFormat('ru-RU', options).format(new Date(value))
}

