import { StoreContext } from "@/store";
import axios from "axios";

export enum UserModuleActions {
  LOAD_CURRENT_USER = "loadCurrentUser"
}



export interface UserData {
  userName: string;
  fullName: string | null;
  jobPosition: string | null;
  hasEmployee: boolean;
  roles: string;
  plan: string | null;
}

export class UserState {
  constructor(public user: {}) {}
}

const UserStore = {
  namespaced: true,
  state: new UserState({}),

  mutations: {
    loadCurrentUser(state: UserState, user: UserData) {
      state.user = user;
    },

    clearUser(state: UserState) {
      state.user = {};
    }
  },
  actions: {
    async loadCurrentUser(context: StoreContext<UserState>) {
      await axios.get("users/get-user-data").then(response => {
        const user = response.data;

        context.commit(UserModuleActions.LOAD_CURRENT_USER, user);
      });
    }
  }
};

export default UserStore;
