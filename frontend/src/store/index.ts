import Vue from "vue";
import Vuex, {ActionContext} from "vuex";
import UserModule from "@/store/modules/UserModule";


Vue.use(Vuex);

export type StoreContext<T> = ActionContext<T, any>;

export default new Vuex.Store({
    modules: {
        user: UserModule
    }
});
