import Vue from "vue";
import VueRouter, {RouteConfig} from "vue-router";
import DefaultLayout from "@/components/layouts/DefaultLayout.vue";
import HomePage from "@/components/pages/HomePage.vue";
import LoginWindow from "@/components/pages/LoginWindow.vue";
import AccountEditor from "@/components/pages/catalogs/account/AccountEditor.vue";
import AccountList from "@/components/pages/catalogs/account/AccountList.vue";
import EmployeeList from "@/components/pages/catalogs/employees/EmployeeList.vue";
import ClientList from "@/components/pages/catalogs/clients/ClientList.vue";
import DealList from "@/components/pages/catalogs/deals/DealList.vue";
import NomenList from "@/components/pages/catalogs/nomen/NomenList.vue";
import EmployeeEditor from "@/components/pages/catalogs/employees/EmployeeEditor.vue";
import ClientEditor from "@/components/pages/catalogs/clients/ClientEditor.vue";
import DealEditor from "@/components/pages/catalogs/deals/DealEditor.vue"
import NomenEditor from "@/components/pages/catalogs/nomen/NomenEditor.vue";
import SummaryReport from "@/components/pages/catalogs/reports/SummaryReport.vue";
import BoardForTheCurrentBate from "@/components/pages/catalogs/reports/BoardForTheCurrentBate.vue";
import PaymentPlanForTomorrow from "@/components/pages/catalogs/reports/PaymentPlanForTomorrow.vue";
import TheFactOfThePaymentsForToday from "@/components/pages/catalogs/reports/TheFactOfThePaymentsForToday.vue";
import FactOfPaymentsForAWeek from "@/components/pages/catalogs/reports/FactOfPaymentsForAWeek.vue";
import dateFilter from "@/components/filters/date.filter";

Vue.filter('date', dateFilter);

Vue.use(VueRouter);

const Dummy = {
  template: `
    <router-view/>
  `
};

const routes: Array<RouteConfig> = [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      { path: "", component: HomePage },

      { path: "/employees", component: EmployeeList },
      { path: "/employees/new", component: EmployeeEditor },
      { path: "/employees/edit/:id", component: EmployeeEditor, props: true },

      { path: "/clients", component: ClientList },
      { path: "/clients/new", component: ClientEditor },
      { path: "/clients/edit/:id", component: ClientEditor, props: true },

      { path: "/deals", component: DealList },
      { path: "/deals/new", component: DealEditor },
      { path: "/deals/edit/:id", component: DealEditor, props: true },

      { path: "/nomen", component: NomenList },
      { path: "/nomen/new", component: NomenEditor },
      { path: "/nomen/edit/:id", component: NomenEditor, props: true },

      { path: "/users", component: AccountList },
      { path: "/users/new", component: AccountEditor },
      { path: "/users/edit/:id", component: AccountEditor, props: true },

      { path: "/summaryReport", component: SummaryReport },

      { path: "/boardForTheCurrentBate", component: BoardForTheCurrentBate },


      { path: "/taymentPlanForTomorrow", component: PaymentPlanForTomorrow },


      { path: "/theFactOfThePaymentsForToday", component: TheFactOfThePaymentsForToday },

      { path: "/factOfPaymentsForAWeek", component: FactOfPaymentsForAWeek }

    ]
  },
  {
    path: "/login",
    component: LoginWindow
  }
];

const router = new VueRouter({
  // mode: "history",
  routes
});

export default router;
