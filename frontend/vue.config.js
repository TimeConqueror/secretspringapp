// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");

module.exports = {
  devServer: {
    proxy: "http://localhost:8090"
  },
  css: { modules: true },
  filenameHashing: false,
  outputDir: path.resolve(__dirname, "../src/main/resources/templates/"),
  configureWebpack: {
    plugins: [new HardSourceWebpackPlugin()]
  }
};
